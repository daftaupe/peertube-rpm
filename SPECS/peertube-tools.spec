%define debug_package %{nil}
%global __strip /bin/true
%define repo github.com/Chocobozzz/PeerTube
%define _name PeerTube
%define _build_id_links none

Name:           peertube-tools
Version:        4.3.0
Release:        2%{?dist}
Summary:        CLI tools for PeerTube

License:        AGPLv3
URL:            https://%{repo}
Source0:        https://%{repo}/archive/refs/tags/v%{version}.zip

Requires:       openssl nodejs ffmpeg >= 3 yarnpkg
BuildRequires:  nodejs python yarnpkg systemd git
Suggests:       redis

%if 0%{?fedora}
BuildRequires:  gcc-c++ npm yarnpkg
%endif

%if 0%{?fedora}
BuildRequires:  gcc-c++ npm yarnpkg
%endif

AutoReq:        no 
AutoReqProv:    no

%description
CLI tools for PeerTube

%prep
%setup -q -c -n %{_name}-%{version}

%build
cd %{_name}-%{version}
NOCLIENT=1 yarn install --pure-lockfile
npm run setup:cli

%install
mkdir -p %{buildroot}%{_datadir}

cp -a %{_name}-%{version} %{buildroot}%{_datadir}/%{name}

%files
%{_datadir}/%{name}
%license %{_name}-%{version}/LICENSE
%doc %{_name}-%{version}/support/doc

%changelog
* Wed Oct 5 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 4.3.0-2
- Disable build-id generation

* Sun Oct 2 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 4.3.0-1
- Initial version of tools 4.3.0
