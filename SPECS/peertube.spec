%define debug_package %{nil}
%global __strip /bin/true
%define repo github.com/Chocobozzz/PeerTube
%define _build_id_links none

Name:           peertube
Version:        4.3.0
Release:        2%{?dist}
Summary:        Federated (ActivityPub) video streaming platform using P2P (BitTorrent) directly in the web browser with WebTorrent and Angular

License:        AGPLv3
URL:            https://%{repo}
Source0:        https://%{repo}/releases/download/v%{version}/%{name}-v%{version}.zip

Requires:       openssl nodejs ffmpeg >= 3 yarnpkg
BuildRequires:  nodejs python yarnpkg systemd git gcc-g++
Suggests:       redis

AutoReq:        no 
AutoReqProv:    no

%description
Federated (ActivityPub) video streaming platform using P2P (BitTorrent) directly in the web browser with WebTorrent and Angular

%prep
%setup -q -c -n %{name}-v%{version}

%build
cd %{name}-v%{version}
yarn install --pure-lockfile

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_datadir}
mkdir -p %{buildroot}%{_sysconfdir}/%{name}

cp %{name}-v%{version}/support/systemd/%{name}.service %{buildroot}%{_unitdir}
sed -i  "s@/var/www/%{name}/config@/etc/%{name}@;s@/var/www/%{name}/%{name}-latest@/usr/share/%{name}@g" "%{buildroot}%{_unitdir}/%{name}.service"
cp %{name}-v%{version}/config/production.yaml.example %{buildroot}%{_sysconfdir}/%{name}/production.yaml
sed -i "s@/var/www/%{name}@/var/lib/%{name}@g" "%{buildroot}%{_sysconfdir}/%{name}/production.yaml"

cp -a %{name}-v%{version} %{buildroot}%{_datadir}/%{name}
rm -rf %{buildroot}%{_datadir}/%{name}/{config,*.md,LICENSE}

%post
if [ "$1" = 1 ]; then
    groupadd --system peertube
    useradd --system --gid peertube -d /var/lib/peertube -s /usr/bin/nologin peertube
    mkdir /var/lib/peertube
    chown -R peertube:peertube /var/lib/peertube 
fi

%postun
if [ "$1" = 0 ]; then
    userdel -f peertube
    systemctl daemon-reload
	mv /var/lib/peertube /var/lib/peertube.bkp
fi

%files
%{_datadir}/%{name}
%{_unitdir}/%{name}.service
%config(noreplace) %{_sysconfdir}/%{name}/production.yaml
%license %{name}-v%{version}/LICENSE
%doc %{name}-v%{version}/support/doc

%changelog
* Wed Oct 5 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 4.3.0-2
- Disable build-id generation

* Sun Oct 2 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 4.3.0-1
- Update to version 4.3.0

* Wed Mar 30 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 4.1.1-1
- Update to version 4.1.1

* Sat Sep 19 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 2.4.0-1
- Update to version 2.4.0

* Sat Sep 19 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 2.3.0-1
- Update to version 2.3.0

* Tue Feb 11 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 2.1.0-1
- Update to version 2.1.0

* Tue Nov 12 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 2.0.0-1
- Update to version 2.0.0

* Mon Sep 09 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.4.1-1
- Update to version 1.4.1

* Wed Aug 28 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.4.0-1
- Update to version 1.4.0

* Mon Jun 17 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.3.1-1
- Update to version 1.3.1

* Mon Jun 03 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.3.0-1
- Update to version 1.3.0

* Wed Feb 27 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.2.1-2
- Install dependencies for remote tools

* Thu Feb 14 2019 Rigel KENT <sendmemail@rigelk.eu> 1.2.1-1
- Update to version 1.2.1

* Wed Feb 06 2019 Rigel KENT <sendmemail@rigelk.eu> 1.2.0-1
- Update to version 1.2.0

* Tue Dec 04 2018 Rigel KENT <sendmemail@rigelk.eu> 1.1.0-1
- Update to version 1.1.0

* Thu Oct 18 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.1.0-alpha1-1
- Update to version 1.1.0-alpha1
- Remove autorestart behaviour

* Thu Oct 11 2018 Rigel KENT <sendmemail@rigelk.eu> 1.0.0-1
- Update to version 1.0.0

* Wed Oct 10 2018 Rigel KENT <sendmemail@rigelk.eu> 1.0.0-rc2-1
- Update to version 1.0.0-rc2

* Mon Oct 01 2018 Rigel KENT <sendmemail@rigelk.eu> 1.0.0-beta16-1
- Update to version 1.0.0-beta16

* Wed Sep 26 2018 Rigel KENT <sendmemail@rigelk.eu> 1.0.0-beta15-1
- Update to version 1.0.0-beta15

* Tue Sep 25 2018 Rigel KENT <sendmemail@rigelk.eu> 1.0.0-beta14-1
- Update to version 1.0.0-beta14

* Thu Sep 13 2018 Rigel KENT <sendmemail@rigelk.eu> 1.0.0-beta13-1
- Update to version 1.0.0-beta13

* Wed Sep 12 2018 Rigel KENT <sendmemail@rigelk.eu> 1.0.0-beta12-1
- Update to version 1.0.0-beta12

* Tue Aug 21 2018 Rigel KENT <sendmemail@rigelk.eu> 1.0.0-beta11-1
- Update to version 1.0.0-beta11

* Mon Aug 13 2018 Rigel KENT <sendmemail@rigelk.eu> 1.0.0-beta10-1
- Update to version 1.0.0-beta10

* Mon Jun 25 2018 Rigel KENT <sendmemail@rigelk.eu> 1.0.0-beta9-1
- Update to version 1.0.0-beta9

* Tue Jun 12 2018 Rigel KENT <sendmemail@rigelk.eu> 1.0.0-beta8-1
- Update to version 1.0.0-beta8

* Tue May 29 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.0.0-beta7-1
- Update to version 1.0.0-beta7

* Wed May 23 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.0.0-beta6-1
- Update to version 1.0.0-beta6

* Mon May 07 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.0.0-beta4-1
- Update to version 1.0.0-beta4

* Sat Apr 14 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.0.0-beta3-2
- Inclusion of CentOS7 specific part

* Thu Apr 12 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.0.0-beta3-1
- Initial rpm : version 1.0.0-beta3
